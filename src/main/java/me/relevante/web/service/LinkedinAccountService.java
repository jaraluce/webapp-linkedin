package me.relevante.web.service;

import me.relevante.api.LinkedinApi;
import me.relevante.api.OAuthKeyPair;
import me.relevante.api.OAuthTokenPair;
import me.relevante.model.LinkedinFullUser;
import me.relevante.model.LinkedinProfile;
import me.relevante.model.RelevanteAccount;
import me.relevante.network.Linkedin;
import me.relevante.web.model.RegisterResult;
import me.relevante.web.model.oauth.LinkedinOAuthManager;
import me.relevante.web.model.oauth.NetworkOAuthManager;
import me.relevante.web.persistence.RelevanteAccountRepo;
import me.relevante.web.persistence.RelevanteContextRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Service;

@Service
public class LinkedinAccountService extends AbstractNetworkAccountService<Linkedin, LinkedinProfile, LinkedinFullUser>
        implements NetworkAccountService<Linkedin, LinkedinFullUser> {

    private OAuthKeyPair<Linkedin> oAuthConsumerKeyPair;

    @Autowired
    public LinkedinAccountService(RelevanteAccountRepo relevanteAccountRepo,
                                  RelevanteContextRepo relevanteContextRepo,
                                  CrudRepository<LinkedinFullUser, String> fullUserRepo,
                                  QueueService queueService,
                                  OAuthKeyPair<Linkedin> oAuthConsumerKeyPair) {
        super(relevanteAccountRepo, relevanteContextRepo, fullUserRepo, queueService, Linkedin.getInstance());
        this.oAuthConsumerKeyPair = oAuthConsumerKeyPair;
    }

    @Override
    public Linkedin getNetwork() {
        return Linkedin.getInstance();
    }

    @Override
    public RegisterResult<LinkedinFullUser> registerBasicAuthNetworkAccount(String relevanteId,
                                                                            String url,
                                                                            String username,
                                                                            String password) {
        throw new UnsupportedOperationException();
    }

    @Override
    protected NetworkOAuthManager<Linkedin> createNetworkOAuthManager() {
        return new LinkedinOAuthManager(oAuthConsumerKeyPair);
    }

    @Override
    protected LinkedinProfile createProfileFromOAuthData(OAuthTokenPair<Linkedin> accessTokenPair) {
        return new LinkedinApi(oAuthConsumerKeyPair, accessTokenPair).getUserData();
    }

    @Override
    protected LinkedinProfile createProfileFromBasicAuthData(String s, String s1, String s2) {
        throw new UnsupportedOperationException();
    }

    @Override
    protected LinkedinFullUser createFullUserFromProfile(LinkedinProfile profile) {
        return new LinkedinFullUser(profile);
    }

    @Override
    protected void populateRelevanteAccountWithNetworkData(RelevanteAccount relevanteAccount,
                                                           LinkedinFullUser fullUser) {
        if (relevanteAccount.getName() == null) {
            relevanteAccount.setName(fullUser.getProfile().getName());
        }
        if (relevanteAccount.getEmail() == null) {
            relevanteAccount.setEmail(fullUser.getProfile().getEmail());
        }
        if (relevanteAccount.getImageUrl() == null) {
            relevanteAccount.setImageUrl(fullUser.getProfile().getImageUrl());
        }

    }

}
