package me.relevante.web.service;

import me.relevante.model.Blacklist;
import me.relevante.model.LinkedinActionComment;
import me.relevante.model.LinkedinActionConnectRequest;
import me.relevante.model.LinkedinActionInmailMessage;
import me.relevante.model.LinkedinActionLike;
import me.relevante.model.LinkedinFullPost;
import me.relevante.model.LinkedinFullUser;
import me.relevante.model.LinkedinGroup;
import me.relevante.model.RelevanteAccount;
import me.relevante.model.RelevanteContext;
import me.relevante.model.filter.FilterChain;
import me.relevante.model.filter.ValidationFilter;
import me.relevante.network.Linkedin;
import me.relevante.network.Network;
import me.relevante.web.model.filter.OutOfBlacklistFilter;
import me.relevante.web.model.validation.LinkedinFullUserValidator;
import me.relevante.web.persistence.LinkedinActionCommentRepo;
import me.relevante.web.persistence.LinkedinActionConnectRequestRepo;
import me.relevante.web.persistence.LinkedinActionInmailMessageRepo;
import me.relevante.web.persistence.LinkedinActionLikeRepo;
import me.relevante.web.persistence.LinkedinFullPostRepo;
import me.relevante.web.persistence.LinkedinFullUserRepo;
import me.relevante.web.persistence.LinkedinGroupRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

@Service
public class LinkedinWatchlistUserService implements NetworkWatchlistUserService<Linkedin, LinkedinFullUser> {

    private final LinkedinFullUserRepo fullUserRepo;
    private final LinkedinFullPostRepo fullPostRepo;
    private final LinkedinGroupRepo groupRepo;
    private final LinkedinActionConnectRequestRepo connectRequestRepo;
    private final LinkedinActionInmailMessageRepo inmailMessageRepo;
    private final LinkedinActionLikeRepo likeRepo;
    private final LinkedinActionCommentRepo commentRepo;
    private final LinkedinFullUserValidator fullUserValidator;

    @Autowired
    public LinkedinWatchlistUserService(LinkedinFullUserRepo fullUserRepo,
                                        LinkedinFullPostRepo fullPostRepo,
                                        LinkedinGroupRepo groupRepo,
                                        LinkedinActionConnectRequestRepo connectRequestRepo,
                                        LinkedinActionInmailMessageRepo inmailMessageRepo,
                                        LinkedinActionLikeRepo likeRepo,
                                        LinkedinActionCommentRepo commentRepo,
                                        LinkedinFullUserValidator fullUserValidator) {
        this.fullUserRepo = fullUserRepo;
        this.fullPostRepo = fullPostRepo;
        this.groupRepo = groupRepo;
        this.connectRequestRepo = connectRequestRepo;
        this.inmailMessageRepo = inmailMessageRepo;
        this.likeRepo = likeRepo;
        this.commentRepo = commentRepo;
        this.fullUserValidator = fullUserValidator;
    }

    @Override
    public Network getNetwork() {
        return Linkedin.getInstance();
    }

    public List<LinkedinFullUser> getNetworkWatchlistUsers(RelevanteAccount relevanteAccount,
                                                           RelevanteContext relevanteContext,
                                                           String watchlistId) {

        // Get full users in this watchlist ID
        List<String> watchlistUserIds = relevanteContext.getWatchlistUserIdsInWatchlistByNetwork(watchlistId, Linkedin.getInstance());
        List<LinkedinFullUser> linkedinFullUsers = fullUserRepo.findByIdIn(watchlistUserIds);

        // Select users out of blacklist, validated
        Blacklist blacklist = relevanteContext.getBlacklist();
        List<LinkedinFullUser> selectedLinkedinUsers = new FilterChain<LinkedinFullUser>()
                .add(new OutOfBlacklistFilter(blacklist))
                .add(new ValidationFilter<>(fullUserValidator))
                .execute(linkedinFullUsers);

        Map<String, LinkedinFullPost> fullPostsById = assignFullPostsToFullUsers(selectedLinkedinUsers);
        if (relevanteAccount.isNetworkConnected(Linkedin.getInstance())) {
            String relevanteUserLinkedinId = relevanteAccount.getCredentials(Linkedin.getInstance()).getUserId();
            populateFullUsersWithGroups(selectedLinkedinUsers);
            populateFullUsersWithActions(selectedLinkedinUsers, relevanteUserLinkedinId);
            populateFullPostsWithActions(fullPostsById, relevanteUserLinkedinId);
        }

        return selectedLinkedinUsers;
    }

    private Map<String, LinkedinFullPost> assignFullPostsToFullUsers(List<LinkedinFullUser> fullUsers) {
        Set<String> postIds = new HashSet();
        fullUsers.forEach(searchUser -> postIds.addAll(searchUser.getPostIds()));
        List<LinkedinFullPost> posts = fullPostRepo.findByIdIn(new ArrayList<>(postIds));
        Map<String, LinkedinFullPost> fullPostsById = new HashMap<>();
        posts.forEach(post -> fullPostsById.put(post.getId(), post));
        for (LinkedinFullUser fullUser : fullUsers) {
            fullUser.getLastPosts().clear();
            for (String postId : fullUser.getPostIds()) {
                fullUser.getLastPosts().add(fullPostsById.get(postId));
            }
        }
        return fullPostsById;
    }

    private void populateFullUsersWithActions(List<LinkedinFullUser> fullUsers,
                                              String relevanteUserLinkedinId) {
        List<String> userIds = fullUsers.stream().map(fullUser -> fullUser.getId()).collect(Collectors.toList());
        List<LinkedinActionConnectRequest> connectRequests = connectRequestRepo.findByAuthorIdAndTargetUserIdIn(relevanteUserLinkedinId, userIds);
        List<LinkedinActionInmailMessage> inmailMessages = inmailMessageRepo.findByAuthorIdAndTargetUserIdIn(relevanteUserLinkedinId, userIds);
        Map<String, LinkedinFullUser> fullUsersById = new HashMap<>();
        fullUsers.forEach(fullUser -> fullUsersById.put(fullUser.getId(), fullUser));
        connectRequests.forEach(connectRequest -> fullUsersById.get(connectRequest.getTargetUserId()).getConnectRequests().add(connectRequest));
        inmailMessages.forEach(inmailMessage -> fullUsersById.get(inmailMessage.getTargetUserId()).getInmailMessages().add(inmailMessage));
    }

    private void populateFullPostsWithActions(Map<String, LinkedinFullPost> fullPostsById,
                                              String relevanteUserLinkedinId) {
        List<String> postIds = fullPostsById.values().stream().map(fullPost -> fullPost.getId()).collect(Collectors.toList());
        List<LinkedinActionLike> likes = likeRepo.findByAuthorIdAndPostIdIn(relevanteUserLinkedinId, postIds);
        List<LinkedinActionComment> comments = commentRepo.findByAuthorIdAndPostIdIn(relevanteUserLinkedinId, postIds);
        likes.forEach(like -> fullPostsById.get(like.getPostId()).getLikes().add(like));
        comments.forEach(comment -> fullPostsById.get(comment.getPostId()).getComments().add(comment));
    }

    private void populateFullUsersWithGroups(List<LinkedinFullUser> fullUsers) {
        Set<String> groupIds = new HashSet<>();
        for (LinkedinFullUser fullUser : fullUsers) {
            fullUser.getGroupIds().forEach(groupId -> groupIds.add(groupId));
        }
        List<LinkedinGroup> groups = groupRepo.findByIdIn(groupIds);
        Map<String, LinkedinGroup> groupById = new HashMap<>();
        groups.forEach(group -> groupById.put(group.getId(), group));
        for (LinkedinFullUser fullUser : fullUsers) {
            fullUser.getGroups().clear();
            for (String groupId : fullUser.getGroupIds()) {
                LinkedinGroup group = groupById.get(groupId);
                fullUser.getGroups().add(group);
            }
        }
    }

}
