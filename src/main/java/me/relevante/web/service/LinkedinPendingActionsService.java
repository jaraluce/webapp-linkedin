package me.relevante.web.service;

import me.relevante.model.ActionStatus;
import me.relevante.model.LinkedinActionComment;
import me.relevante.model.LinkedinActionConnectRequest;
import me.relevante.model.LinkedinActionInmailMessage;
import me.relevante.model.LinkedinActionLike;
import me.relevante.network.Linkedin;
import me.relevante.web.model.json.LinkedinCommentJson;
import me.relevante.web.model.json.LinkedinConnectRequestJson;
import me.relevante.web.model.json.LinkedinInmailMessageJson;
import me.relevante.web.model.json.LinkedinLikeJson;
import me.relevante.web.model.json.NetworkActionJson;
import me.relevante.web.persistence.LinkedinActionCommentRepo;
import me.relevante.web.persistence.LinkedinActionConnectRequestRepo;
import me.relevante.web.persistence.LinkedinActionInmailMessageRepo;
import me.relevante.web.persistence.LinkedinActionLikeRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by daniel-ibanez on 22/09/16.
 */
@Service
public class LinkedinPendingActionsService implements NetworkPendingActionsService<Linkedin> {


    private LinkedinActionLikeRepo likeRepo;
    private LinkedinActionCommentRepo commentRepo;
    private LinkedinActionConnectRequestRepo connectRequestRepo;
    private LinkedinActionInmailMessageRepo inmailMessageRepo;

    @Autowired
    public LinkedinPendingActionsService(LinkedinActionLikeRepo likeRepo,
                                         LinkedinActionCommentRepo commentRepo,
                                         LinkedinActionConnectRequestRepo connectRequestRepo,
                                         LinkedinActionInmailMessageRepo inmailMessageRepo) {
        this.likeRepo = likeRepo;
        this.commentRepo = commentRepo;
        this.connectRequestRepo = connectRequestRepo;
        this.inmailMessageRepo = inmailMessageRepo;
    }

    @Override
    public Linkedin getNetwork() {
        return Linkedin.getInstance();
    }

    @Override
    public List<NetworkActionJson<Linkedin>> getPendingActions(String userId) {
        List<NetworkActionJson<Linkedin>> pendingActions = new ArrayList<>();
        List<LinkedinLikeJson> likes = getPendingLikes(userId);
        pendingActions.addAll(likes);
        List<LinkedinCommentJson> comments = getPendingComments(userId);
        pendingActions.addAll(comments);
        List<LinkedinConnectRequestJson> connectRequests = getPendingConnectRequests(userId);
        pendingActions.addAll(connectRequests);
        List<LinkedinInmailMessageJson> inmailMessages = getPendingInmailMessages(userId);
        pendingActions.addAll(inmailMessages);
        return pendingActions;
    }

    @Override
    public void acknowledgeActionDone(String actionName, String actionId) {
        if (actionName.equals(LinkedinLikeJson.ACTION_NAME)) {
            LinkedinActionLike like = likeRepo.findOne(actionId);
            like.setSuccess();
            likeRepo.save(like);
        } else if (actionName.equals(LinkedinCommentJson.ACTION_NAME)) {
            LinkedinActionComment comment = commentRepo.findOne(actionId);
            comment.setSuccess();
            commentRepo.save(comment);
        } else if (actionName.equals(LinkedinConnectRequestJson.ACTION_NAME)) {
            LinkedinActionConnectRequest connectRequest = connectRequestRepo.findOne(actionId);
            connectRequest.setSuccess();
            connectRequestRepo.save(connectRequest);
        } else if (actionName.equals(LinkedinInmailMessageJson.ACTION_NAME)) {
            LinkedinActionInmailMessage inmailMessage = inmailMessageRepo.findOne(actionId);
            inmailMessage.setSuccess();
            inmailMessageRepo.save(inmailMessage);
        } else {
            throw new IllegalArgumentException("Invalid actionName received [" + actionName + "]");
        }
    }

    @Override
    public void acknowledgeActionError(String actionName, String actionId) {
        if (actionName.equals(LinkedinLikeJson.ACTION_NAME)) {
            LinkedinActionLike like = likeRepo.findOne(actionId);
            like.setError();
            likeRepo.save(like);
        } else if (actionName.equals(LinkedinCommentJson.ACTION_NAME)) {
            LinkedinActionComment comment = commentRepo.findOne(actionId);
            comment.setError();
            commentRepo.save(comment);
        } else if (actionName.equals(LinkedinConnectRequestJson.ACTION_NAME)) {
            LinkedinActionConnectRequest connectRequest = connectRequestRepo.findOne(actionId);
            connectRequest.setError();
            connectRequestRepo.save(connectRequest);
        } else if (actionName.equals(LinkedinInmailMessageJson.ACTION_NAME)) {
            LinkedinActionInmailMessage inmailMessage = inmailMessageRepo.findOne(actionId);
            inmailMessage.setError();
            inmailMessageRepo.save(inmailMessage);
        } else {
            throw new IllegalArgumentException("Invalid actionName received [" + actionName + "]");
        }
    }

    private List<LinkedinLikeJson> getPendingLikes(String authorId) {
        List<LinkedinActionLike> likes = likeRepo.findByAuthorIdAndStatusNot(authorId, ActionStatus.PROCESSED.name());
        List<LinkedinLikeJson> likeJsons = new ArrayList<>();
        for (LinkedinActionLike like : likes) {
            LinkedinLikeJson likeJson = new LinkedinLikeJson(like.getId().toString(), like.getPostId());
            likeJsons.add(likeJson);
        }
        return likeJsons;
    }

    private List<LinkedinCommentJson> getPendingComments(String authorId) {
        List<LinkedinActionComment> comments = commentRepo.findByAuthorIdAndStatusNot(authorId, ActionStatus.PROCESSED.name());
        List<LinkedinCommentJson> commentJsons = new ArrayList<>();
        for (LinkedinActionComment comment : comments) {
            LinkedinCommentJson commentJson = new LinkedinCommentJson(comment.getId().toString(), comment.getPostId(), comment.getComment());
            commentJsons.add(commentJson);
        }
        return commentJsons;
    }

    private List<LinkedinConnectRequestJson> getPendingConnectRequests(String authorId) {
        List<LinkedinActionConnectRequest> connectRequests = connectRequestRepo.findByAuthorIdAndStatusNot(authorId, ActionStatus.PROCESSED.name());
        List<LinkedinConnectRequestJson> connectRequestJsons = new ArrayList<>();
        for (LinkedinActionConnectRequest connectRequest : connectRequests) {
            LinkedinConnectRequestJson connectRequestJson = new LinkedinConnectRequestJson(connectRequest.getId().toString(), connectRequest.getTargetUserId());
            connectRequestJsons.add(connectRequestJson);
        }
        return connectRequestJsons;
    }

    private List<LinkedinInmailMessageJson> getPendingInmailMessages(String authorId) {
        List<LinkedinActionInmailMessage> inmailMessages = inmailMessageRepo.findByAuthorIdAndStatusNot(authorId, ActionStatus.PROCESSED.name());
        List<LinkedinInmailMessageJson> inmailMessageJsons = new ArrayList<>();
        for (LinkedinActionInmailMessage inmailMessage : inmailMessages) {
            LinkedinInmailMessageJson inmailMessageJson = new LinkedinInmailMessageJson(inmailMessage.getId().toString(), inmailMessage.getTargetUserId(), inmailMessage.getMessageText());
            inmailMessageJsons.add(inmailMessageJson);
        }
        return inmailMessageJsons;
    }

}
