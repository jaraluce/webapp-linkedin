package me.relevante.web.service;

import me.relevante.model.Blacklist;
import me.relevante.model.LinkedinFullPost;
import me.relevante.model.LinkedinFullUser;
import me.relevante.model.RelevanteAccount;
import me.relevante.model.RelevanteContext;
import me.relevante.model.filter.FilterChain;
import me.relevante.model.filter.ValidationFilter;
import me.relevante.network.Linkedin;
import me.relevante.web.model.FullUserWithPostsComparator;
import me.relevante.web.model.filter.IsNotLoggedUserFilter;
import me.relevante.web.model.filter.OutOfBlacklistFilter;
import me.relevante.web.model.validation.LinkedinFullUserValidator;
import me.relevante.web.persistence.LinkedinFullPostRepo;
import me.relevante.web.persistence.LinkedinFullUserRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

@Service
public class LinkedinRecentActivityService implements NetworkRecentActivityService<Linkedin, LinkedinFullUser> {

    private LinkedinFullUserRepo linkedinFullUserRepo;
    private LinkedinFullPostRepo linkedinFullPostRepo;
    private FullUserWithPostsComparator<Linkedin, LinkedinFullUser> networkFullUserComparator;
    private LinkedinFullUserValidator fullUserValidator;

    @Autowired
    public LinkedinRecentActivityService(LinkedinFullUserRepo linkedinFullUserRepo,
                                         LinkedinFullPostRepo linkedinFullPostRepo,
                                         FullUserWithPostsComparator<Linkedin, LinkedinFullUser> networkFullUserComparator,
                                         LinkedinFullUserValidator fullUserValidator) {
        this.linkedinFullUserRepo = linkedinFullUserRepo;
        this.linkedinFullPostRepo = linkedinFullPostRepo;
        this.networkFullUserComparator = networkFullUserComparator;
        this.fullUserValidator = fullUserValidator;
    }

    @Override
    public Linkedin getNetwork() {
        return Linkedin.getInstance();
    }

    public List<LinkedinFullUser> getNetworkRecentlyUpdatedUsers(RelevanteAccount relevanteAccount,
                                                                RelevanteContext relevanteContext) {

        List<String> relatedUserIds = relevanteContext.getRelatedUserIdsByNetwork(Linkedin.getInstance());
        List<LinkedinFullUser> linkedinFullUsers = linkedinFullUserRepo.findByIdIn(relatedUserIds);

        assignLinkedinPostsToUsers(linkedinFullUsers);

        // Sort by most recent activity
        List<LinkedinFullUser> sortedLinkedinFullUsers = new ArrayList<>(linkedinFullUsers);
        sortedLinkedinFullUsers.sort(networkFullUserComparator);

        // Select users out of blacklist, validated and in pagination
        Blacklist blacklist = relevanteContext.getBlacklist();
        List<LinkedinFullUser> selectedUsers = new FilterChain<LinkedinFullUser>()
                .add(new OutOfBlacklistFilter(blacklist))
                .add(new IsNotLoggedUserFilter(relevanteAccount))
                .add(new ValidationFilter<>(fullUserValidator))
                .execute(sortedLinkedinFullUsers);

        return selectedUsers;
    }

    private void assignLinkedinPostsToUsers(List<LinkedinFullUser> fullUsers) {
        Set<String> postIds = new HashSet();
        fullUsers.forEach(searchUser -> postIds.addAll(searchUser.getPostIds()));
        List<LinkedinFullPost> posts = linkedinFullPostRepo.findByIdIn(new ArrayList<>(postIds));
        Map<String, LinkedinFullPost> postMap = new HashMap<>();
        posts.forEach(post -> postMap.put(post.getId(), post));
        for (LinkedinFullUser fullUser : fullUsers) {
            fullUser.getLastPosts().clear();
            for (String postId : fullUser.getPostIds()) {
                fullUser.getLastPosts().add(postMap.get(postId));
            }
        }
    }

}
