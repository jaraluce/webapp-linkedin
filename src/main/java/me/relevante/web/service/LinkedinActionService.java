package me.relevante.web.service;

import me.relevante.api.NetworkCredentials;
import me.relevante.model.ActionResult;
import me.relevante.model.ActionStatus;
import me.relevante.model.LinkedinActionComment;
import me.relevante.model.LinkedinActionConnectRequest;
import me.relevante.model.LinkedinActionInmailMessage;
import me.relevante.model.LinkedinActionLike;
import me.relevante.model.RelevanteAccount;
import me.relevante.network.Linkedin;
import me.relevante.web.model.ActionProgressOutput;
import me.relevante.web.model.ActionServiceResponse;
import me.relevante.web.persistence.LinkedinActionCommentRepo;
import me.relevante.web.persistence.LinkedinActionConnectRequestRepo;
import me.relevante.web.persistence.LinkedinActionInmailMessageRepo;
import me.relevante.web.persistence.LinkedinActionLikeRepo;
import me.relevante.web.persistence.RelevanteAccountRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class LinkedinActionService implements NetworkActionService<Linkedin> {

    private RelevanteAccountRepo relevanteAccountRepo;
    private LinkedinActionLikeRepo likeRepo;
    private LinkedinActionCommentRepo commentRepo;
    private LinkedinActionConnectRequestRepo connectRequestRepo;
    private LinkedinActionInmailMessageRepo inmailMessageRepo;
    private ActionProgressOutput actionProgressOutput;
    private QueueService queueService;

    @Autowired
    public LinkedinActionService(RelevanteAccountRepo relevanteAccountRepo,
                                 LinkedinActionLikeRepo likeRepo,
                                 LinkedinActionCommentRepo commentRepo,
                                 LinkedinActionConnectRequestRepo connectRequestRepo,
                                 LinkedinActionInmailMessageRepo inmailMessageRepo,
                                 ActionProgressOutput actionProgressOutput,
                                 QueueService queueService) {
        this.relevanteAccountRepo = relevanteAccountRepo;
        this.likeRepo = likeRepo;
        this.commentRepo = commentRepo;
        this.connectRequestRepo = connectRequestRepo;
        this.inmailMessageRepo = inmailMessageRepo;
        this.actionProgressOutput = actionProgressOutput;
        this.queueService = queueService;
    }

    @Override
    public Linkedin getNetwork() {
        return Linkedin.getInstance();
    }

    @Override
    public String putLevel1Action(String relevanteId,
                                  String postId) {
        return putLike(relevanteId, postId);
    }

    @Override
    public String postLevel2Action(String relevanteId,
                                   String postId,
                                   String commentText) {
        return postComment(relevanteId, postId, commentText);
    }

    @Override
    public String putLevel3Action(String relevanteId,
                                  String targetUserId) {
        return putConnectRequest(relevanteId, targetUserId);
    }

    @Override
    public String postLevel4Action(String relevanteId,
                                   String targetUserId,
                                   String subject,
                                   String message) {
        return postInmailMessage(relevanteId, targetUserId, subject, message);
    }

    public String putLike(String relevanteId,
                          String postId) {
        ActionServiceResponse serviceResponse = processLike(relevanteId, postId);
        return actionProgressOutput.getOutput(serviceResponse.getStatus(), serviceResponse.getResult());
    }

    public String postComment(String relevanteId,
                              String postId,
                              String commentText) {
        ActionServiceResponse serviceResponse = processComment(relevanteId, postId, commentText);
        return actionProgressOutput.getOutput(serviceResponse.getStatus(), serviceResponse.getResult());
    }

    public String putConnectRequest(String relevanteId,
                                   String targetUserId) {
        ActionServiceResponse serviceResponse = processConnectRequest(relevanteId, targetUserId);
        return actionProgressOutput.getOutput(serviceResponse.getStatus(), serviceResponse.getResult());
    }

    public String postInmailMessage(String relevanteId,
                                    String targetUserId,
                                    String subject,
                                    String message) {
        ActionServiceResponse serviceResponse = processInmailMessage(relevanteId, targetUserId, subject, message);
        return actionProgressOutput.getOutput(serviceResponse.getStatus(), serviceResponse.getResult());
    }

    private ActionServiceResponse processLike(String relevanteId,
                                              String postId) {

        RelevanteAccount relevanteAccount = relevanteAccountRepo.findOne(relevanteId);
        NetworkCredentials<Linkedin> credentials = relevanteAccount.getCredentials(Linkedin.getInstance());
        if (credentials == null) {
            return new ActionServiceResponse(ActionStatus.PROCESSED, ActionResult.ERROR);
        }

        List<LinkedinActionLike> existingPostLikes = likeRepo.findByAuthorIdAndPostId(credentials.getUserId(), postId);
        if (existingPostLikes.size() > 0) {
            return new ActionServiceResponse(existingPostLikes.get(0));
        }

        LinkedinActionLike newPostLike = new LinkedinActionLike(credentials.getUserId(), postId);
        newPostLike.setUserManaged(); // TODO To be removed when the extension works
        likeRepo.save(newPostLike);
        queueService.sendBulkActionsMessage(relevanteId, LinkedinActionLike.class, newPostLike.getId());
        return new ActionServiceResponse(newPostLike);
    }


    private ActionServiceResponse processComment(String relevanteId,
                                                 String postId,
                                                 String commentText) {

        RelevanteAccount relevanteAccount = relevanteAccountRepo.findOne(relevanteId);
        NetworkCredentials<Linkedin> credentials = relevanteAccount.getCredentials(Linkedin.getInstance());
        if (credentials == null) {
            return new ActionServiceResponse(ActionStatus.PROCESSED, ActionResult.ERROR);
        }

        List<LinkedinActionComment> existingPostComments = commentRepo.findByAuthorIdAndPostId(credentials.getUserId(), postId);
        if (existingPostComments.size() > 0) {
            return new ActionServiceResponse(existingPostComments.get(0));
        }

        LinkedinActionComment newPostComment = new LinkedinActionComment(credentials.getUserId(), postId, commentText);
        newPostComment.setUserManaged(); // TODO To be removed when the extension works
        commentRepo.save(newPostComment);
        queueService.sendBulkActionsMessage(relevanteId, LinkedinActionComment.class, newPostComment.getId());
        return new ActionServiceResponse(newPostComment);
    }

    private ActionServiceResponse processConnectRequest(String relevanteId,
                                                        String targetUserId) {

        RelevanteAccount relevanteAccount = relevanteAccountRepo.findOne(relevanteId);
        NetworkCredentials<Linkedin> credentials = relevanteAccount.getCredentials(Linkedin.getInstance());
        if (credentials == null) {
            return new ActionServiceResponse(ActionStatus.PROCESSED, ActionResult.ERROR);
        }

        List<LinkedinActionConnectRequest> existingConnectRequests = connectRequestRepo.findByAuthorIdAndTargetUserId(credentials.getUserId(), targetUserId);
        if (existingConnectRequests.size() > 0) {
            return new ActionServiceResponse(existingConnectRequests.get(0));
        }

        LinkedinActionConnectRequest newConnectRequest = new LinkedinActionConnectRequest(credentials.getUserId(), targetUserId);
        newConnectRequest.setUserManaged(); // TODO To be removed when the extension works
        connectRequestRepo.save(newConnectRequest);
        queueService.sendBulkActionsMessage(relevanteId, LinkedinActionConnectRequest.class, newConnectRequest.getId());
        return new ActionServiceResponse(newConnectRequest);
    }

    private ActionServiceResponse processInmailMessage(String relevanteId,
                                                       String targetUserId,
                                                       String subject,
                                                       String message) {

        RelevanteAccount relevanteAccount = relevanteAccountRepo.findOne(relevanteId);
        NetworkCredentials<Linkedin> credentials = relevanteAccount.getCredentials(Linkedin.getInstance());
        if (credentials == null) {
            return new ActionServiceResponse(ActionStatus.PROCESSED, ActionResult.ERROR);
        }

        List<LinkedinActionInmailMessage> existingInmailMessages = inmailMessageRepo.findByAuthorIdAndTargetUserId(credentials.getUserId(), targetUserId);
        if (existingInmailMessages.size() > 0) {
            return new ActionServiceResponse(existingInmailMessages.get(0));
        }

        LinkedinActionInmailMessage newInmailMessage = new LinkedinActionInmailMessage(credentials.getUserId(), targetUserId, subject, message);
        newInmailMessage.setUserManaged(); // TODO To be removed when the extension works
        inmailMessageRepo.save(newInmailMessage);
        queueService.sendBulkActionsMessage(relevanteId, LinkedinActionInmailMessage.class, newInmailMessage.getId());
        return new ActionServiceResponse(newInmailMessage);
    }

}
