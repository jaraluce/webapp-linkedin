package me.relevante.web.service;

import me.relevante.model.LinkedinFullUser;
import me.relevante.model.RelevanteAccount;
import me.relevante.model.RelevanteContext;
import me.relevante.model.filter.FilterChain;
import me.relevante.model.filter.ValidationFilter;
import me.relevante.network.Linkedin;
import me.relevante.web.model.FullUserWithPostsComparator;
import me.relevante.web.model.filter.IsNotLoggedUserFilter;
import me.relevante.web.model.validation.LinkedinFullUserValidator;
import me.relevante.web.persistence.LinkedinFullUserRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class LinkedinBlacklistUserService implements NetworkBlacklistUserService<Linkedin, LinkedinFullUser> {

    private LinkedinFullUserRepo fullUserRepo;
    private FullUserWithPostsComparator<Linkedin, LinkedinFullUser> fullUserComparator;
    private LinkedinFullUserValidator fullUserValidator;

    @Autowired
    public LinkedinBlacklistUserService(LinkedinFullUserRepo fullUserRepo,
                                        FullUserWithPostsComparator<Linkedin, LinkedinFullUser> fullUserComparator,
                                        LinkedinFullUserValidator fullUserValidator) {
        this.fullUserRepo = fullUserRepo;
        this.fullUserComparator = fullUserComparator;
        this.fullUserValidator = fullUserValidator;
    }

    @Override
    public Linkedin getNetwork() {
        return Linkedin.getInstance();
    }

    public List<LinkedinFullUser> getNetworkBlacklistUsers(RelevanteAccount relevanteAccount,
                                                           RelevanteContext relevanteContext) {

        List<String> relatedUserIds = relevanteContext.getBlacklistUserIdsByNetwork(Linkedin.getInstance());
        List<LinkedinFullUser> linkedinFullUsers = fullUserRepo.findByIdIn(relatedUserIds);

        // Sort by most recent activity
        List<LinkedinFullUser> sortedLinkedinFullUsers = new ArrayList<>(linkedinFullUsers);
        sortedLinkedinFullUsers.sort(fullUserComparator);

        // Select users out of blacklist, validated and in pagination
        List<LinkedinFullUser> selectedLinkedinUsers = new FilterChain<LinkedinFullUser>()
                .add(new IsNotLoggedUserFilter<>(relevanteAccount))
                .add(new ValidationFilter<>(fullUserValidator))
                .execute(sortedLinkedinFullUsers);

        return selectedLinkedinUsers;

    }

}
