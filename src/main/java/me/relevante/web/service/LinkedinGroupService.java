package me.relevante.web.service;

import me.relevante.api.OAuthKeyPair;
import me.relevante.network.Linkedin;
import me.relevante.web.model.ActionProgressOutput;
import me.relevante.web.persistence.RelevanteAccountRepo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class LinkedinGroupService {

    private static final Logger logger = LoggerFactory.getLogger(LinkedinGroupService.class);

    private RelevanteAccountRepo relevanteAccountRepo;
    private OAuthKeyPair<Linkedin> linkedinOAuthConsumerKeyPair;
    private ActionProgressOutput actionProgressOutput;

    @Autowired
    public LinkedinGroupService(RelevanteAccountRepo relevanteAccountRepo,
                                OAuthKeyPair<Linkedin> linkedinOAuthConsumerKeyPair,
                                ActionProgressOutput actionProgressOutput) {
        this.relevanteAccountRepo = relevanteAccountRepo;
        this.linkedinOAuthConsumerKeyPair = linkedinOAuthConsumerKeyPair;
        this.actionProgressOutput = actionProgressOutput;
    }

    public String requestMembership(String relevanteId,
                                    String groupId) {
        throw new UnsupportedOperationException();
    }

}
