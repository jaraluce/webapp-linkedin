package me.relevante.web.model.json;

import me.relevante.network.Linkedin;

/**
 * Created by daniel-ibanez on 22/09/16.
 */
public class LinkedinInmailMessageJson extends LinkedinActionJson implements NetworkActionJson<Linkedin> {

    public static final String ACTION_NAME = "inmailMessage";

    private String targetUserId;
    private String messageText;

    public LinkedinInmailMessageJson(String id,
                                     String targetUserId,
                                     String messageText) {
        super(id);
        this.action = ACTION_NAME;
        this.targetUserId = targetUserId;
        this.messageText = messageText;
    }

    public String getTargetUserId() {
        return targetUserId;
    }

    public String getMessageText() {
        return messageText;
    }
}
