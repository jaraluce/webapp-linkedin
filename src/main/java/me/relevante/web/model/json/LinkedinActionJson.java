package me.relevante.web.model.json;

import me.relevante.network.Linkedin;

/**
 * Created by daniel-ibanez on 22/09/16.
 */
public class LinkedinActionJson extends NetworkAbstractActionJson<Linkedin> implements NetworkActionJson<Linkedin> {

    public LinkedinActionJson(String id) {
        super(id);
        this.network = Linkedin.getInstance().getName();
    }
}
