package me.relevante.web.model.json;

import me.relevante.network.Linkedin;

/**
 * Created by daniel-ibanez on 22/09/16.
 */
public class LinkedinCommentJson extends LinkedinActionJson implements NetworkActionJson<Linkedin> {

    public static final String ACTION_NAME = "comment";

    private String postId;
    private String commentText;

    public LinkedinCommentJson(String id,
                               String postId,
                               String commentText) {
        super(id);
        this.action = ACTION_NAME;
        this.postId = postId;
        this.commentText = commentText;
    }

    public String getPostId() {
        return postId;
    }

    public String getCommentText() {
        return commentText;
    }
}
