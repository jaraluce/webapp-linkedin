package me.relevante.web.model.json.card;

import me.relevante.network.Linkedin;

import java.util.ArrayList;
import java.util.List;

public class LinkedinCardUser extends AbstractNetworkCardUser<Linkedin> implements NetworkCardUser<Linkedin> {

    private String name;
    private String first_name;
    private String headline;
    private String summary;
    private String url;
    private String img;
    private String location;
    private String industry;
    private String companyName;
    private List<LinkedinCardPost> shares;
    private String connectRequest;
    private String inmailMessage;
    private List<String> skills;
    private List<LinkedinCardPosition> experience;
    private List<LinkedinCardGroup> groups;

    public LinkedinCardUser() {
        super();
        this.shares = new ArrayList<>();
        this.skills = new ArrayList<>();
        this.experience = new ArrayList<>();
        this.groups = new ArrayList<>();
    }

    @Override
    public Linkedin getNetwork() {
        return Linkedin.getInstance();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFirstName() {
        return first_name;
    }

    public void setFirstName(String firstName) {
        this.first_name = firstName;
    }

    public String getHeadline() {
        return headline;
    }

    public void setHeadline(String headline) {
        this.headline = headline;
    }

    public String getSummary() {
        return summary;
    }

    public void setSummary(String summary) {
        this.summary = summary;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getIndustry() {
        return industry;
    }

    public void setIndustry(String industry) {
        this.industry = industry;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public List<LinkedinCardPost> getShares() {
        return shares;
    }

    public String getConnectRequest() {
        return connectRequest;
    }

    public void setConnectRequest(String connectRequest) {
        this.connectRequest = connectRequest;
    }

    public String getInmailMessage() {
        return inmailMessage;
    }

    public void setInmailMessage(String inmailMessage) {
        this.inmailMessage = inmailMessage;
    }

    public List<String> getSkills() {
        return skills;
    }

    public List<LinkedinCardPosition> getExperience() {
        return experience;
    }

    public List<LinkedinCardGroup> getGroups() {
        return groups;
    }
}
