package me.relevante.web.model.json;

import me.relevante.model.LinkedinProfile;
import me.relevante.network.Linkedin;
import org.json.simple.JSONObject;
import org.springframework.stereotype.Component;

@Component
public class LinkedinLoggedUserJsonMapper implements NetworkLoggedUserJsonMapper<Linkedin, LinkedinProfile> {

    @Override
    public Linkedin getNetwork() {
        return Linkedin.getInstance();
    }

    @Override
    public JSONObject mapNetworkProfileToJson(LinkedinProfile profile) {
        JSONObject jsonProfile = new JSONObject();
        jsonProfile.put("name_linkedin", profile.getName());
        jsonProfile.put("firstName", profile.getFirstName());
        jsonProfile.put("lastName", profile.getLastName());
        jsonProfile.put("picture", profile.getImageUrl());
        jsonProfile.put("email", profile.getEmail());
        jsonProfile.put("headline", profile.getHeadline());
        jsonProfile.put("profile_url", profile.getProfileUrl());
        jsonProfile.put("industry", profile.getIndustry());
        jsonProfile.put("location", profile.getArea() + ", " + profile.getCountry());
        return jsonProfile;
    }
}