package me.relevante.web.model.json.card;

import me.relevante.network.Linkedin;
import me.relevante.network.Network;

import java.util.ArrayList;
import java.util.List;

public class LinkedinCardPost implements NetworkCardPost {

    private String id;
    private String title;
    private String text;
    private String url;
    private String url_picture;
    private String post_img;
    private List<LinkedinCardComment> prospects;
    private List<LinkedinCardUser> usersLike;
    private String date;
    private String authorId;
    private String like;
    private String comment;
    private String group_description;
    private String group_logo;
    private String group_name;

    public LinkedinCardPost() {
        this.prospects = new ArrayList<>();
        this.usersLike = new ArrayList<>();
    }

    @Override
    public Network getNetwork() {
        return Linkedin.getInstance();
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getUrl_picture() {
        return url_picture;
    }

    public void setUrl_picture(String url_picture) {
        this.url_picture = url_picture;
    }

    public String getPost_img() {
        return post_img;
    }

    public void setPost_img(String post_img) {
        this.post_img = post_img;
    }

    public List<LinkedinCardComment> getProspects() {
        return prospects;
    }

    public List<LinkedinCardUser> getUsersLike() {
        return usersLike;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getAuthorId() {
        return authorId;
    }

    public void setAuthorId(String authorId) {
        this.authorId = authorId;
    }

    public String getLike() {
        return like;
    }

    public void setLike(String like) {
        this.like = like;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getGroup_description() {
        return group_description;
    }

    public void setGroup_description(String group_description) {
        this.group_description = group_description;
    }

    public String getGroup_logo() {
        return group_logo;
    }

    public void setGroup_logo(String group_logo) {
        this.group_logo = group_logo;
    }

    public String getGroup_name() {
        return group_name;
    }

    public void setGroup_name(String group_name) {
        this.group_name = group_name;
    }
}
