package me.relevante.web.model.json.card;

public class LinkedinCardGroup {

    private String name;
    private String url;
    private String group_img;

    public LinkedinCardGroup() {}

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getImageUrl() {
        return group_img;
    }

    public void setImageUrl(String imageUrl) {
        this.group_img = imageUrl;
    }
}
