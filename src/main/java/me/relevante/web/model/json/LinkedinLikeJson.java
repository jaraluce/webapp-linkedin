package me.relevante.web.model.json;

import me.relevante.network.Linkedin;

/**
 * Created by daniel-ibanez on 22/09/16.
 */
public class LinkedinLikeJson extends LinkedinActionJson implements NetworkActionJson<Linkedin> {

    public static final String ACTION_NAME = "like";

    private String postId;

    public LinkedinLikeJson(String id,
                            String postId) {
        super(id);
        this.action = ACTION_NAME;
        this.postId = postId;
    }

    public String getPostId() {
        return postId;
    }

}
