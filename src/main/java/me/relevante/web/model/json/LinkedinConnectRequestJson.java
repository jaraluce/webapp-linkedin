package me.relevante.web.model.json;

import me.relevante.network.Linkedin;

/**
 * Created by daniel-ibanez on 22/09/16.
 */
public class LinkedinConnectRequestJson extends LinkedinActionJson implements NetworkActionJson<Linkedin> {

    public static final String ACTION_NAME = "connect";

    private String targetUserId;

    public LinkedinConnectRequestJson(String id,
                                      String targetUserId) {
        super(id);
        this.action = ACTION_NAME;
        this.targetUserId = targetUserId;
    }

    public String getTargetUserId() {
        return targetUserId;
    }

}
