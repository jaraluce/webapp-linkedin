package me.relevante.web.model.json.card;

public class LinkedinCardComment {

    private String comment;
    private String userId;
    private LinkedinCardUser userComment;

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public LinkedinCardUser getUserComment() {
        return userComment;
    }

    public void setUserComment(LinkedinCardUser userComment) {
        this.userComment = userComment;
    }
}
