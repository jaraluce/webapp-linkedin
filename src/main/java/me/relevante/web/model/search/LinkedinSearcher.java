package me.relevante.web.model.search;

import me.relevante.model.LinkedinFullPost;
import me.relevante.model.LinkedinFullUser;
import me.relevante.model.LinkedinGroup;
import me.relevante.model.LinkedinSearchUser;
import me.relevante.model.LinkedinSignalGroupMembership;
import me.relevante.model.NetworkSignal;
import me.relevante.model.SignalCategory;
import me.relevante.network.Linkedin;
import me.relevante.nlp.LinkedinStemUserIndex;
import me.relevante.nlp.core.NlpCore;
import me.relevante.web.persistence.LinkedinFullPostRepo;
import me.relevante.web.persistence.LinkedinFullUserRepo;
import me.relevante.web.persistence.LinkedinGroupRepo;
import me.relevante.web.persistence.LinkedinSearchUserRepo;
import me.relevante.web.persistence.LinkedinStemUserIndexRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

@Component
public class LinkedinSearcher extends AbstractNetworkSearcher<Linkedin, LinkedinFullUser, LinkedinSearchUser, LinkedinStemUserIndex>
        implements NetworkSearcher<Linkedin, LinkedinSearchUser> {

    private static final int MAX_GROUPS = 12;

    private LinkedinStemUserIndexRepo stemUserIndexRepo;
    private LinkedinFullPostRepo fullPostRepo;
    private LinkedinFullUserRepo fullUserRepo;
    private LinkedinSearchUserRepo searchUserRepo;
    private LinkedinGroupRepo groupRepo;

    @Autowired
    public LinkedinSearcher(NlpCore nlpCore,
                            LinkedinStemUserIndexRepo stemUserIndexRepo,
                            LinkedinFullPostRepo fullPostRepo,
                            LinkedinFullUserRepo fullUserRepo,
                            LinkedinSearchUserRepo searchUserRepo,
                            LinkedinGroupRepo groupRepo) {
        super(nlpCore);
        this.stemUserIndexRepo = stemUserIndexRepo;
        this.fullPostRepo = fullPostRepo;
        this.fullUserRepo = fullUserRepo;
        this.searchUserRepo = searchUserRepo;
        this.groupRepo = groupRepo;
    }

    @Override
    public Linkedin getNetwork() {
        return Linkedin.getInstance();
    }

    @Override
    public List<LinkedinSearchUser> execute(List<String> searchTerms) {
        List<LinkedinSearchUser> resultUsers = super.execute(searchTerms);
        assignGroupsToUsers(resultUsers);
        return resultUsers;
    }

    @Override
    protected List<LinkedinStemUserIndex> findStemUserIndexByStem(String stem, int maxUsers) {
        return stemUserIndexRepo.findByStemOrderByScoreDesc(stem, new PageRequest(0, maxUsers));
    }

    @Override
    protected List<LinkedinSearchUser> findSearchUsersById(List<String> ids) {
        return searchUserRepo.findByIdIn(ids);
    }

    @Override
    protected List<LinkedinFullUser> findFullUsersById(List<String> ids) {
        return fullUserRepo.findByIdIn(ids);
    }

    @Override
    protected void assignPostsToUsers(List<LinkedinSearchUser> searchUsers, int maxPosts) {
        // Create set for each user containing post IDs from their signals
        List<LinkedHashSet<String>> allUsersPostIds = new ArrayList<>();
        for (LinkedinSearchUser searchUser : searchUsers) {
            LinkedHashSet<String> postIds = new LinkedHashSet();
            allUsersPostIds.add(postIds);
            for (NetworkSignal signal : searchUser.getSignals()) {
                if (signal.getCategory().equals(SignalCategory.CONTENT_ACTIVITY)) {
                    postIds.add(signal.getRelatedEntityId());
                }
            }
        }
        Set<String> allPostIds = new HashSet<>();
        for (int i = 0; i < allUsersPostIds.size(); i++) {
            // Add all Post IDs from the full user
            LinkedHashSet<String> userPostIds = allUsersPostIds.get(i);
            List<String> fullUserPostIds = searchUsers.get(i).getFullUser().getPostIds();
            userPostIds.addAll(fullUserPostIds);
            // Leave posts to the max allowed
            List<String> maxAllowedUserPostIds = new ArrayList<>();
            Iterator<String> iterator = userPostIds.iterator();
            for (int j = 0; j < maxPosts; j++) {
                if (!iterator.hasNext())
                    break;
                String postId = iterator.next();
                maxAllowedUserPostIds.add(postId);
                allPostIds.add(postId);
            }
            userPostIds.clear();
            userPostIds.addAll(maxAllowedUserPostIds);
        }

        List<LinkedinFullPost> fullPosts = fullPostRepo.findByIdIn(allPostIds);
        Map<String, LinkedinFullPost> fullPostsById = new HashMap<>();
        fullPosts.forEach(fullPost -> fullPostsById.put(fullPost.getId(), fullPost));
        for (int i = 0; i < searchUsers.size(); i++) {
            LinkedinSearchUser searchUser = searchUsers.get(i);
            LinkedHashSet<String> postIds = allUsersPostIds.get(i);
            Iterator<String> iterator = postIds.iterator();
            searchUser.getPosts().clear();
            while (iterator.hasNext()) {
                String postId = iterator.next();
                LinkedinFullPost fullPost = fullPostsById.get(postId);
                searchUser.getPosts().add(fullPost);
            }
        }
    }

    private void assignGroupsToUsers(List<LinkedinSearchUser> searchUsers) {
        // Create set for each user containing post IDs from their signals
        Set<String> groupIds = new HashSet<>();
        for (LinkedinSearchUser searchUser : searchUsers) {
            searchUser.getFullUser().getGroupIds().clear();
            for (NetworkSignal signal : searchUser.getSignals()) {
                if (signal.getClass().equals(LinkedinSignalGroupMembership.class)) {
                    String groupId = signal.getRelatedEntityId();
                    searchUser.getFullUser().getGroupIds().add(groupId);
                    groupIds.add(groupId);
                }
            }
        }
        List<LinkedinGroup> groups = groupRepo.findByIdIn(groupIds);
        Map<String, LinkedinGroup> groupById = new HashMap<>();
        groups.forEach(group -> groupById.put(group.getId(), group));
        for (LinkedinSearchUser searchUser : searchUsers) {
            searchUser.getGroups().clear();
            for (String groupId : searchUser.getFullUser().getGroupIds()) {
                LinkedinGroup group = groupById.get(groupId);
                searchUser.getFullUser().getGroups().add(group);
            }
        }
    }

}
