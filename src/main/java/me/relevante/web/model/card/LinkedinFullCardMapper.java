package me.relevante.web.model.card;

import me.relevante.api.NetworkCredentials;
import me.relevante.model.LinkedinFullPost;
import me.relevante.model.LinkedinFullUser;
import me.relevante.model.LinkedinGroup;
import me.relevante.model.LinkedinPosition;
import me.relevante.model.LinkedinPost;
import me.relevante.model.LinkedinProfile;
import me.relevante.model.RelevanteAccount;
import me.relevante.model.RelevanteContext;
import me.relevante.network.Linkedin;
import me.relevante.web.model.json.card.LinkedinCardGroup;
import me.relevante.web.model.json.card.LinkedinCardPosition;
import me.relevante.web.model.json.card.LinkedinCardPost;
import me.relevante.web.model.json.card.LinkedinCardUser;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;

@Component
public class LinkedinFullCardMapper extends AbstractNetworkFullCardMapper<Linkedin, LinkedinFullUser, LinkedinCardUser>
        implements NetworkFullCardMapper<Linkedin, LinkedinFullUser, LinkedinCardUser> {

    @Override
    public Linkedin getNetwork() {
        return Linkedin.getInstance();
    }

    @Override
    public LinkedinCardUser mapFullUser(LinkedinFullUser fullUser,
                                        RelevanteAccount relevanteAccount,
                                        RelevanteContext relevanteContext) {

        LinkedinCardUser cardUser = mapProfile(fullUser.getProfile(), relevanteContext);
        cardUser.setId(fullUser.getId());
        cardUser.getKeywords().addAll(getCardRelatedTerms(fullUser.getRelatedTerms()));
        cardUser.getSkills().addAll(fullUser.getSkills());
        for (LinkedinPosition position : fullUser.getPositions()) {
            LinkedinCardPosition cardPosition = mapPosition(position);
            cardUser.getExperience().add(cardPosition);
        }
        for (LinkedinFullPost fullPost : fullUser.getLastPosts()) {
            LinkedinCardPost cardPost = mapFullPost(fullPost);
            if (cardPost != null) {
                cardUser.getShares().add(cardPost);
                assignPostEngageProperties(cardPost, fullPost, relevanteAccount);
            }
        }
        for (LinkedinGroup group : fullUser.getGroups()) {
            LinkedinCardGroup cardGroup = mapGroup(group);
            if (group != null) {
                cardUser.getGroups().add(cardGroup);
            }
        }
        assignUserEngageProperties(cardUser, fullUser, relevanteAccount);

        return cardUser;
    }

    public LinkedinCardUser mapProfile(LinkedinProfile profile,
                                       RelevanteContext relevanteContext) {

        LinkedinCardUser cardUser = new LinkedinCardUser();
        cardUser.setType(profile.getNetwork().getName());
        cardUser.setIsContact(relevanteContext.isContact(profile));
        cardUser.getNotes().addAll(mapTags(relevanteContext.getTagNamesByUser(profile)));
        cardUser.getWatchlists().addAll(mapWatchlists(relevanteContext.getWatchlistNamesByUser(profile)));

        cardUser.setName(StringUtils.defaultString(profile.getName()));
        cardUser.setFirstName(StringUtils.defaultString(profile.getFirstName()));
        cardUser.setHeadline(StringUtils.defaultString(profile.getHeadline()));
        cardUser.setSummary(StringUtils.defaultString(profile.getPositionSummary(), profile.getSummary()));
        cardUser.setUrl(StringUtils.defaultString(profile.getProfileUrl()));
        cardUser.setImg(StringUtils.defaultString(profile.getImageUrl()));
        cardUser.setLocation(getLocation(profile.getArea(), profile.getCountry()));
        cardUser.setIndustry(StringUtils.defaultString(profile.getIndustry()));
        cardUser.setCompanyName(StringUtils.defaultString(profile.getCompany()));

        return cardUser;
    }

    private void assignUserEngageProperties(LinkedinCardUser cardUser,
                                            LinkedinFullUser fullUser,
                                            RelevanteAccount relevanteAccount) {

        NetworkCredentials credentials = relevanteAccount.getCredentials(Linkedin.getInstance());
        if (credentials == null) {
            return;
        }
        cardUser.setConnectRequest(networkActionProgressOutput.getOutput(fullUser.findConnectRequestsByAuthorId(credentials.getUserId())));
        cardUser.setInmailMessage(networkActionProgressOutput.getOutput(fullUser.findInmailMessagesByAuthorId(credentials.getUserId())));
        for (LinkedinFullPost fullPost : fullUser.getLastPosts()) {
            if (fullPost.findLikesByAuthorId(credentials.getUserId()).size() > 0) {
                cardUser.setTapDone(true);
            }
            if (fullPost.findCommentsByAuthorId(credentials.getUserId()).size() > 0) {
                cardUser.setTouchDone(true);
            }
        }
    }

    private void assignPostEngageProperties(LinkedinCardPost cardPost,
                                            LinkedinFullPost fullPost,
                                            RelevanteAccount relevanteAccount) {

        NetworkCredentials credentials = relevanteAccount.getCredentials(Linkedin.getInstance());
        if (credentials == null) {
            return;
        }
        cardPost.setLike(networkActionProgressOutput.getOutput(fullPost.findLikesByAuthorId(credentials.getUserId())));
        cardPost.setComment(networkActionProgressOutput.getOutput(fullPost.findCommentsByAuthorId(credentials.getUserId())));
    }

    public LinkedinCardPost mapFullPost(LinkedinFullPost fullPost) {

        LinkedinCardPost cardPost = mapPost(fullPost.getPost());
        LinkedinGroup group = fullPost.getGroup();
        if (group != null){
            cardPost.setGroup_description(StringUtils.defaultString(group.getDescription()));
            cardPost.setGroup_logo(StringUtils.defaultString(group.getImageUrl()));
            cardPost.setGroup_name(StringUtils.defaultString(group.getName()));
        }
        cardPost.getUsersLike().clear();
        cardPost.getProspects().clear();

		return cardPost;
	}

    public LinkedinCardPost mapPost(LinkedinPost post) {

        LinkedinCardPost cardPost = new LinkedinCardPost();
        cardPost.setId(post.getId());
        cardPost.setTitle(getShareTitle(post.getTitle(), post.getSubTitle()));
        cardPost.setText(getShareText(post.getContent(), post.getSubContent()));
        cardPost.setUrl(getShareUrl(post));
        cardPost.setUrl_picture(StringUtils.defaultString(post.getImageUrl()));
        cardPost.setPost_img(post.getImageUrl());
        cardPost.setDate(dateFormatter.formatDate(post.getCreationTimestamp()));
        cardPost.setAuthorId(post.getAuthorId());

        return cardPost;
    }

    public LinkedinCardGroup mapGroup(LinkedinGroup group) {
        LinkedinCardGroup cardGroup = new LinkedinCardGroup();
        cardGroup.setName(group.getName());
        cardGroup.setUrl(group.getUrl());
        cardGroup.setImageUrl(group.getImageUrl());
        return cardGroup;
    }


    private String getShareTitle(String title, String subtitle) {

		String jsonTitle = "";
		if (StringUtils.isNotBlank(title))
			jsonTitle = title;
		else if (StringUtils.isNotBlank(subtitle))
			jsonTitle = subtitle;

		return jsonTitle;
	}

    private String getShareText(String text, String subtext) {

		String jsonText = "";
		if (text != null) {
			String[] textArray = text.split("[\\r\\n ]");
			if (textArray.length == 1 && textArray[0].contains("http://"))
				jsonText = subtext;
			else
				jsonText = text;
		}

		return jsonText;
	}

    private String getShareUrl(LinkedinPost linkedinPost) {

		// If a Linkedin Group post, return post URL
		if (linkedinPost.getGroupId() != null) {
            String postId = linkedinPost.getId();
            if (postId.contains("g") && postId.contains("S")) {
                postId = linkedinPost.getId().replace("g-", "").replace("-S-", "-");
            }
			String url = "https://www.linkedin.com/groups/" + linkedinPost.getGroupId() + "/" + postId;
            return url;
		}

        int topicPosition = linkedinPost.getId().lastIndexOf("-");
        String topicId = linkedinPost.getId().substring(topicPosition + 1);
		String url = "https://www.linkedin.com/nhome/updates?topic=" + topicId;
		return url;
	}

    private String getLocation(String country, String area) {

		String location = "";
		if (StringUtils.isNotBlank(country)) {
			if (StringUtils.isNotBlank(area))
				location = area + ", " + country;
			else
				location = country;
		}
		else if (StringUtils.isNotEmpty(area)) {
			location = area;
		}

		return location;
	}

    private LinkedinCardPosition mapPosition(LinkedinPosition position) {
        LinkedinCardPosition cardPosition = new LinkedinCardPosition();
        cardPosition.setTitle(position.getTitle());
        cardPosition.setDescription(position.getDescription());
        cardPosition.setOrganizationName(position.getOrganizationName());
        cardPosition.setOrganizationUrl(position.getOrganizationUrl());
        return cardPosition;
    }
}
