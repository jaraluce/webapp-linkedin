package me.relevante.web.model.card;

import me.relevante.model.LinkedinFullPost;
import me.relevante.model.LinkedinGroup;
import me.relevante.model.LinkedinPosition;
import me.relevante.model.LinkedinPost;
import me.relevante.model.LinkedinSearchUser;
import me.relevante.model.LinkedinSignalBioDescription;
import me.relevante.model.LinkedinSignalBioHeadline;
import me.relevante.model.LinkedinSignalCurrentPositionSummary;
import me.relevante.model.LinkedinSignalCurrentPositionTitle;
import me.relevante.model.LinkedinSignalGroupMembership;
import me.relevante.model.LinkedinSignalPosition;
import me.relevante.model.LinkedinSignalPostAuthorship;
import me.relevante.model.LinkedinSignalPostComment;
import me.relevante.model.LinkedinSignalPostLike;
import me.relevante.model.LinkedinSignalShareAuthorship;
import me.relevante.model.LinkedinSignalSkill;
import me.relevante.model.NetworkSignal;
import me.relevante.model.RelevanteAccount;
import me.relevante.model.RelevanteContext;
import me.relevante.model.SignalCategory;
import me.relevante.network.Linkedin;
import me.relevante.web.model.json.card.CardSignal;
import me.relevante.web.model.json.card.LinkedinCardPost;
import me.relevante.web.model.json.card.LinkedinCardUser;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Component
public class LinkedinSearchCardMapper extends AbstractNetworkSearchCardMapper<Linkedin, LinkedinSearchUser, LinkedinCardUser>
        implements NetworkSearchCardMapper<Linkedin, LinkedinSearchUser, LinkedinCardUser> {

    private static final int MAX_SIGNALS = 4;

    private LinkedinFullCardMapper fullCardMapper;

    @Autowired
    public LinkedinSearchCardMapper(LinkedinFullCardMapper fullCardMapper) {
        this.fullCardMapper = fullCardMapper;
    }

    @Override
    public Linkedin getNetwork() {
        return Linkedin.getInstance();
    }

    @Override
    public LinkedinCardUser mapSearchUser(LinkedinSearchUser searchUser,
                                          RelevanteAccount relevanteAccount,
                                          RelevanteContext relevanteContext) {
        if (searchUser == null) {
            return null;
        }
        LinkedinCardUser cardUser = fullCardMapper.mapFullUser(searchUser.getFullUser(), relevanteAccount, relevanteContext);
        cardUser.getSignals().addAll(mapSignals(searchUser));
        cardUser.getShares().clear();
        for (LinkedinFullPost linkedinFullPost : searchUser.getPosts()) {
            LinkedinCardPost cardPost = mapPost(linkedinFullPost.getPost());
            if (cardPost != null) {
                cardUser.getShares().add(cardPost);
            }
        }
        cardUser.getShares().sort((o1, o2) -> o2.getDate().compareTo(o1.getDate()));
        return cardUser;
    }

    public LinkedinCardPost mapPost(LinkedinPost post) {

        if (post == null) {
            return null;
        }

        LinkedinCardPost cardPost = new LinkedinCardPost();
        cardPost.setId(post.getId());
        cardPost.setTitle(getShareTitle(post.getTitle(), post.getSubTitle()));
        cardPost.setText(getShareText(post.getContent(), post.getSubContent()));
        cardPost.setUrl(getShareUrl(post));
        cardPost.setUrl_picture(StringUtils.defaultString(post.getImageUrl()));
        cardPost.setPost_img(post.getImageUrl());
        cardPost.setDate(dateFormatter.formatDate(post.getCreationTimestamp()));
        cardPost.setAuthorId(post.getAuthorId());

        return cardPost;
    }

    private String getShareTitle(String title, String subtitle) {

		String jsonTitle = "";
		if (StringUtils.isNotBlank(title))
			jsonTitle = title;
		else if (StringUtils.isNotBlank(subtitle))
			jsonTitle = subtitle;

		return jsonTitle;
	}

    private String getShareText(String text, String subtext) {

		String jsonText = "";
		if (text != null) {
			String[] textArray = text.split("[\\r\\n ]");
			if (textArray.length == 1 && textArray[0].contains("http://"))
				jsonText = subtext;
			else
				jsonText = text;
		}

		return jsonText;
	}

    private String getShareUrl(LinkedinPost linkedinPost) {

		// If a Linkedin Group post, return post URL
		if (linkedinPost.getGroupId() != null) {
            String postId = linkedinPost.getId();
            if (postId.contains("g") && postId.contains("S")) {
                postId = linkedinPost.getId().replace("g-", "").replace("-S-", "-");
            }
			String url = "https://www.linkedin.com/groups/" + linkedinPost.getGroupId() + "/" + postId;
            return url;
		}

        int topicPosition = linkedinPost.getId().lastIndexOf("-");
        String topicId = linkedinPost.getId().substring(topicPosition + 1);
		String url = "https://www.linkedin.com/nhome/updates?topic=" + topicId;
		return url;
	}

    private List<CardSignal> mapSignals(LinkedinSearchUser searchUser) {
        // List to return
        List<CardSignal> cardSignals = new ArrayList<>();

        // Create a map with one entry per category.
        // Each value is the list of that category's signals
        Map<SignalCategory, List<NetworkSignal>> signalsByCategory = new HashMap<>();
        for (SignalCategory signalCategory : SignalCategory.values()) {
            signalsByCategory.put(signalCategory, new ArrayList<>());
        }
        for (NetworkSignal signal : searchUser.getSignals()) {
            List<NetworkSignal> categorySignals = signalsByCategory.get(signal.getCategory());
            categorySignals.add(signal);
        }

        // Loop through the map, moving one signal from each category to the list
        boolean isAllProcessed = false;
        while (!isAllProcessed) {
            isAllProcessed = true;
            for (SignalCategory signalCategory : SignalCategory.values()) {
                List<NetworkSignal> categorySignals = signalsByCategory.get(signalCategory);
                if (!categorySignals.isEmpty()) {
                    NetworkSignal signal = categorySignals.get(0);
                    CardSignal cardSignal = mapSignal(signal, searchUser);
                    cardSignals.add(cardSignal);
                    categorySignals.remove(0);
                    if (!categorySignals.isEmpty()) {
                        isAllProcessed = false;
                    }
                }
            }
        }

        cardSignals = cardSignals.subList(0, Math.min(MAX_SIGNALS, cardSignals.size()));

        return cardSignals;
    }

    private CardSignal mapSignal(NetworkSignal signal,
                                 LinkedinSearchUser searchUser) {
        CardSignal cardSignal = new CardSignal();
        String category = mapSignalCategory(signal.getCategory());
        String type = signal.getClass().getSimpleName().substring(14);
        String description = mapSignalDescription(signal, searchUser);
        cardSignal.setCategory(category);
        cardSignal.setType(type);
        cardSignal.setDescription(description);
        return cardSignal;
    }

    private String mapSignalCategory(SignalCategory category) {
        if (category.equals(SignalCategory.CONTENT_ACTIVITY)) {
            return "social";
        }
        else if (category.equals(SignalCategory.SELF_DESCRIPTION)) {
            return "bio";
        }
        else if (category.equals(SignalCategory.NETWORK)) {
            return "graph";
        }
        else if (category.equals(SignalCategory.OWNERSHIP)) {
            return "habitat";
        }
        return "";
    }

    private String mapSignalDescription(NetworkSignal signal,
                                        LinkedinSearchUser searchUser) {
        Class signalClass = signal.getClass();
        if (signalClass.equals(LinkedinSignalBioHeadline.class)) {
            return searchUser.getFullUser().getProfile().getHeadline();
        }
        else if (signalClass.equals(LinkedinSignalBioDescription.class)) {
            return searchUser.getFullUser().getProfile().getSummary();
        }
        else if (signalClass.equals(LinkedinSignalCurrentPositionTitle.class)) {
            return searchUser.getFullUser().getProfile().getTitle();
        }
        else if (signalClass.equals(LinkedinSignalCurrentPositionSummary.class)) {
            return searchUser.getFullUser().getProfile().getPositionSummary();
        }
        else if (signalClass.equals(LinkedinSignalSkill.class)) {
            int skillIndex = Integer.parseInt(signal.getRelatedEntityId());
            return searchUser.getFullUser().getSkills().get(skillIndex);
        }
        else if (signalClass.equals(LinkedinSignalPosition.class)) {
            int positionIndex = Integer.parseInt(signal.getRelatedEntityId());
            LinkedinPosition position = searchUser.getFullUser().getPositions().get(positionIndex);
            return getSignalPositionDescription(position);
        }
        else if (signalClass.equals(LinkedinSignalGroupMembership.class)) {
            for (LinkedinGroup group : searchUser.getGroups()) {
                if (group.getId().equals(signal.getRelatedEntityId())) {
                    return group.getName();
                }
            }
            return null;
        }
        else if (signalClass.equals(LinkedinSignalPostAuthorship.class)
                || signalClass.equals(LinkedinSignalPostLike.class)
                || signalClass.equals(LinkedinSignalPostComment.class)
                || signalClass.equals(LinkedinSignalShareAuthorship.class)) {
            for (LinkedinFullPost post : searchUser.getPosts()) {
                if (post.getId().equals(signal.getRelatedEntityId())) {
                    String description = getSignalPostContent(post.getPost());
                    return description;
                }
            }
            return null;
        }
        return "";
    }

    private String getSignalPositionDescription(LinkedinPosition position) {
        String content = (position.getTitle() != null) ? position.getTitle() : position.getDescription();
        if (position.getOrganizationName() != null) {
            content += " (" + position.getOrganizationName() + ")";
        }
        return content;
    }

    private String getSignalPostContent(LinkedinPost post) {
        String content = post.getTitle();
        if (post.getContent() == null) {
            content = post.getContent();
        }
        return content;
    }
}
