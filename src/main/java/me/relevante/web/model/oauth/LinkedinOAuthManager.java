package me.relevante.web.model.oauth;

import me.relevante.api.OAuthKeyPair;
import me.relevante.api.OAuthTokenPair;
import me.relevante.network.Linkedin;
import org.scribe.builder.ServiceBuilder;
import org.scribe.builder.api.LinkedInApi;
import org.scribe.model.Token;
import org.scribe.model.Verifier;
import org.scribe.oauth.OAuthService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class LinkedinOAuthManager implements NetworkOAuthManager<Linkedin> {

    private static final Logger logger = LoggerFactory.getLogger(LinkedinOAuthManager.class);

    private OAuthKeyPair<Linkedin> oAuthConsumerKeyPair;

    @Autowired
    public LinkedinOAuthManager(OAuthKeyPair<Linkedin> oAuthConsumerKeyPair) {
        this.oAuthConsumerKeyPair = oAuthConsumerKeyPair;
    }

    @Override
    public Linkedin getNetwork() {
        return Linkedin.getInstance();
    }

    @Override
    public OAuthKeyPair<Linkedin> getOAuthConsumerKeyPair() {
        return oAuthConsumerKeyPair;
    }

    @Override
    public OAuthAccessRequest<Linkedin> createOAuthRequest(String callbackUrl) {

        OAuthService oAuthService = createOAuthService(callbackUrl);
        Token requestToken = oAuthService.getRequestToken();
        String oAuthUrl = oAuthService.getAuthorizationUrl(requestToken);

        OAuthAccessRequest<Linkedin> OAuthAccessRequest = new OAuthAccessRequest<>(requestToken.getToken(),
                requestToken.getSecret(), oAuthUrl);

        return OAuthAccessRequest;
    }

    @Override
    public OAuthTokenPair<Linkedin> obtainAccessToken(String requestTokenString,
                                                                    String requestSecret,
                                                                    String requestVerifierString) {
        try {
            OAuthService oAuthService = createOAuthService(null);
            Token requestToken = new Token(requestTokenString, requestSecret);
            Verifier verifier = new Verifier(requestVerifierString);
            Token accessToken = oAuthService.getAccessToken(requestToken, verifier);
            OAuthTokenPair<Linkedin> oAuthTokenPair = new OAuthTokenPair<>(accessToken.getToken(), accessToken.getSecret());
            return oAuthTokenPair;
        }
        catch (Exception e) {
            logger.error("Error obtaining OAuth access token", e);
            return null;
        }

    }

    private OAuthService createOAuthService(String callbackUrl) {

        ServiceBuilder serviceBuilder = new ServiceBuilder()
                .provider(LinkedInApi.class)
                .apiKey(oAuthConsumerKeyPair.getKey())
                .apiSecret(oAuthConsumerKeyPair.getSecret());
        if (callbackUrl != null) {
            serviceBuilder.callback(callbackUrl);
        }
        OAuthService service = serviceBuilder.build();

        return service;
    }

}
