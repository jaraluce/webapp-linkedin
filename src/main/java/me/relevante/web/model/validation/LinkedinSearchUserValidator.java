package me.relevante.web.model.validation;

import me.relevante.model.LinkedinFullUser;
import me.relevante.model.LinkedinSearchUser;
import me.relevante.network.Linkedin;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Created by daniel-ibanez on 21/07/16.
 */
@Component
public class LinkedinSearchUserValidator extends AbstractNetworkSearchUserValidator<Linkedin, LinkedinSearchUser, LinkedinFullUser>
        implements NetworkSearchUserValidator<Linkedin, LinkedinSearchUser> {

    @Override
    public Linkedin getNetwork() {
        return Linkedin.getInstance();
    }

    @Autowired
    public LinkedinSearchUserValidator(LinkedinFullUserValidator wrappedValidator) {
        super(wrappedValidator);
    }
}
