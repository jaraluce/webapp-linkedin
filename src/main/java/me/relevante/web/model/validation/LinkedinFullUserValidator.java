package me.relevante.web.model.validation;

import me.relevante.model.LinkedinFullUser;
import me.relevante.model.LinkedinProfile;
import me.relevante.network.Linkedin;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Created by daniel-ibanez on 21/07/16.
 */
@Component
public class LinkedinFullUserValidator extends AbstractNetworkFullUserValidator<Linkedin, LinkedinFullUser, LinkedinProfile>
        implements NetworkFullUserValidator<Linkedin, LinkedinFullUser> {

    @Override
    public Linkedin getNetwork() {
        return Linkedin.getInstance();
    }

    @Autowired
    public LinkedinFullUserValidator(LinkedinProfileValidator wrappedValidator) {
        super(wrappedValidator);
    }
}
