package me.relevante.web.model.validation;

import me.relevante.model.LinkedinProfile;
import me.relevante.network.Linkedin;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;

/**
 * Created by daniel-ibanez on 19/07/16.
 */
@Component
public class LinkedinProfileValidator implements NetworkProfileValidator<Linkedin, LinkedinProfile> {

    @Override
    public Linkedin getNetwork() {
        return Linkedin.getInstance();
    }

    @Override
    public boolean isValid(LinkedinProfile profile) {
        if (profile == null) {
            return false;
        }
        if (StringUtils.isBlank(profile.getName())) {
            return false;
        }
        if (StringUtils.isBlank(profile.getHeadline())) {
            return false;
        }
        return true;
    }
}
