package me.relevante.web;

import me.relevante.api.OAuthKeyPair;
import me.relevante.network.Linkedin;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * Created by daniel-ibanez on 24/07/16.
 */
@Configuration
public class LinkedinAppConfig {

    @Bean(name = "linkedinMainOAuthKeyPair")
    public OAuthKeyPair<Linkedin> mainOAuthKeyPair(@Value("${linkedin.oAuthCredentials.main.apiKey}") String key,
                                                   @Value("${linkedin.oAuthCredentials.main.apiSecret}") String secret) {
        return new OAuthKeyPair<>(key, secret);
    }

}
