package me.relevante.web.persistence;

import me.relevante.model.LinkedinFullPost;
import me.relevante.persistence.NetworkFindByIdsRepo;
import me.relevante.persistence.NetworkFullPostRepo;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Collection;
import java.util.List;

@Repository
public interface LinkedinFullPostRepo extends NetworkFullPostRepo<LinkedinFullPost>, NetworkFindByIdsRepo<LinkedinFullPost> {
    @Query("{id:'12345'}")
    List<LinkedinFullPost> getPostsWithLikesAndComments(int maxPosts);
    List<LinkedinFullPost> findByPostAuthorIdIn(Collection<String> authorIds);
    List<LinkedinFullPost> findByPostAuthorId(String authorId);
}
