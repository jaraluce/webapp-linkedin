package me.relevante.web.persistence;

import me.relevante.model.LinkedinSearchUser;
import me.relevante.network.Linkedin;
import me.relevante.persistence.NetworkSearchUserRepo;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface LinkedinSearchUserRepo extends NetworkSearchUserRepo<Linkedin, LinkedinSearchUser>, MongoRepository<LinkedinSearchUser, String> {
    List<LinkedinSearchUser> findByIdIn(List<String> ids);
}