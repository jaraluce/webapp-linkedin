package me.relevante.web.persistence;

import me.relevante.model.LinkedinActionComment;
import me.relevante.network.Linkedin;
import me.relevante.persistence.NetworkPostActionRepo;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.Collection;
import java.util.List;

@Repository
public interface LinkedinActionCommentRepo extends NetworkPostActionRepo<Linkedin, LinkedinActionComment>, MongoRepository<LinkedinActionComment, String> {

	List<LinkedinActionComment> findByAuthorIdAndPostId(String authorId, String postId);
	List<LinkedinActionComment> findByPostId(String postId);
	List<LinkedinActionComment> findByAuthorIdAndPostIdIn(String authorId, Collection<String> postIds);
	List<LinkedinActionComment> findByAuthorIdAndStatusNot(String authorId, String status);

}
