package me.relevante.web.persistence;

import me.relevante.model.LinkedinActionInmailMessage;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.Collection;
import java.util.List;

@Repository
public interface LinkedinActionInmailMessageRepo extends MongoRepository<LinkedinActionInmailMessage, String> {

	List<LinkedinActionInmailMessage> findByAuthorIdAndTargetUserId(String authorId, String targetUserId);
	List<LinkedinActionInmailMessage> findByTargetUserId(String targetUserId);
	List<LinkedinActionInmailMessage> findByAuthorIdAndTargetUserIdIn(String authorId, Collection<String> targetUserIds);
	List<LinkedinActionInmailMessage> findByAuthorIdAndStatusNot(String authorId, String status);

}
