package me.relevante.web.persistence;

import me.relevante.model.LinkedinActionLike;
import me.relevante.network.Linkedin;
import me.relevante.persistence.NetworkPostActionRepo;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.Collection;
import java.util.List;

@Repository
public interface LinkedinActionLikeRepo extends NetworkPostActionRepo<Linkedin, LinkedinActionLike>, MongoRepository<LinkedinActionLike, String> {

	List<LinkedinActionLike> findByAuthorIdAndPostId(String authorId, String postId);
	List<LinkedinActionLike> findByPostId(String postId);
	List<LinkedinActionLike> findByAuthorIdAndPostIdIn(String authorId, Collection<String> postIds);
	List<LinkedinActionLike> findByAuthorIdAndStatusNot(String authorId, String status);
}
