package me.relevante.web.persistence;

import me.relevante.network.Linkedin;
import me.relevante.nlp.LinkedinStemUserIndex;
import me.relevante.persistence.NetworkIndexRepo;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface LinkedinStemUserIndexRepo extends NetworkIndexRepo<Linkedin, LinkedinStemUserIndex> {
    List<LinkedinStemUserIndex> findByUserId(String userId);
    List<LinkedinStemUserIndex> findByStemOrderByScoreDesc(String stem, Pageable pageable);
}