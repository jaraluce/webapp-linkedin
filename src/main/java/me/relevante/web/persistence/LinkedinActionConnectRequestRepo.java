package me.relevante.web.persistence;

import me.relevante.model.LinkedinActionConnectRequest;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.Collection;
import java.util.List;

@Repository
public interface LinkedinActionConnectRequestRepo extends MongoRepository<LinkedinActionConnectRequest, String> {

    List<LinkedinActionConnectRequest> findByAuthorIdAndTargetUserId(String authorId, String targetUserId);
    List<LinkedinActionConnectRequest> findByAuthorIdAndTargetUserIdIn(String authorId, Collection<String> targetUserIds);
    List<LinkedinActionConnectRequest> findByAuthorIdAndStatusNot(String authorId, String status);
}
