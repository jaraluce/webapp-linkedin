package me.relevante.web.persistence;

import me.relevante.model.LinkedinFullUser;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.Collection;
import java.util.List;

@Repository
public interface LinkedinFullUserRepo extends MongoRepository<LinkedinFullUser, String> {
    LinkedinFullUser findOneById(String id);
    List<LinkedinFullUser> findByIdIn(Collection<String> ids);
}