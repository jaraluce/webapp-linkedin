package me.relevante.web.persistence;

import me.relevante.model.LinkedinGroup;
import me.relevante.persistence.NetworkFindByIdsRepo;
import org.springframework.stereotype.Repository;

@Repository
public interface LinkedinGroupRepo extends NetworkFindByIdsRepo<LinkedinGroup> {
}
