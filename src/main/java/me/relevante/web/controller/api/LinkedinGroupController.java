package me.relevante.web.controller.api;

import me.relevante.web.controller.CoreSessionAttribute;
import me.relevante.web.controller.LinkedinRoute;
import me.relevante.web.service.LinkedinGroupService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpSession;

@RestController
@RequestMapping(value = LinkedinRoute.API_GROUP)
public class LinkedinGroupController {

    @Autowired private LinkedinGroupService linkedinGroupService;

    @RequestMapping(value = "/requestMembership",
                    method = RequestMethod.POST)
    public String requestGroupMembership(@PathVariable String groupId,
                                         HttpSession session) {

        String relevanteId = (String) session.getAttribute(CoreSessionAttribute.RELEVANTE_ID);
        String groupResponse = linkedinGroupService.requestMembership(relevanteId, groupId);
        return groupResponse;
    }

}