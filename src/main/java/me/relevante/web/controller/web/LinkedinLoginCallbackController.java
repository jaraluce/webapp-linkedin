package me.relevante.web.controller.web;

import me.relevante.web.controller.CoreRoute;
import me.relevante.web.controller.CoreSessionAttribute;
import me.relevante.web.controller.LinkedinRoute;
import me.relevante.web.model.RegisterResult;
import me.relevante.web.service.LinkedinAccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpSession;

@RestController
public class LinkedinLoginCallbackController {

    @Autowired private LinkedinAccountService accountService;

    @RequestMapping(value = LinkedinRoute.LOGIN_CALLBACK, method = RequestMethod.GET)
    public ModelAndView get(@RequestParam(name = "oauth_token") String oAuthRequestOriginalToken,
                            @RequestParam(name = "oauth_verifier") String oAuthRequestVerifier,
                            HttpSession session) {

        String relevanteId = (String) session.getAttribute(CoreSessionAttribute.RELEVANTE_ID);
        String oAuthRequestToken = (String) session.getAttribute(CoreSessionAttribute.OAUTH_REQUEST_TOKEN);
        String oAuthRequestSecret = (String) session.getAttribute(CoreSessionAttribute.OAUTH_REQUEST_SECRET);
        RegisterResult result = accountService.registerOAuthNetworkAccount(relevanteId, oAuthRequestToken, oAuthRequestSecret, oAuthRequestOriginalToken, oAuthRequestVerifier);
        session.setAttribute(CoreSessionAttribute.RELEVANTE_ID, result.getRelevanteAccount().getId());

        return new ModelAndView("redirect:" + CoreRoute.LOGIN);
    }

}