package me.relevante.web.controller.web;

import me.relevante.web.controller.CoreSessionAttribute;
import me.relevante.web.controller.LinkedinRoute;
import me.relevante.web.model.oauth.LinkedinOAuthManager;
import me.relevante.web.model.oauth.OAuthAccessRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

@RestController
public class LinkedinLoginController {

    @Autowired
    private LinkedinOAuthManager linkedinOAuthManager;

    @RequestMapping(value = LinkedinRoute.LOGIN, method = RequestMethod.GET)
    public ModelAndView get(HttpServletRequest request) {

        String baseUrl = request.getRequestURL().substring(0, request.getRequestURL().indexOf(LinkedinRoute.LOGIN));
        OAuthAccessRequest oAuthAccessRequest = linkedinOAuthManager.createOAuthRequest(baseUrl + LinkedinRoute.LOGIN_CALLBACK);
        HttpSession session = request.getSession();
        session.setAttribute(CoreSessionAttribute.OAUTH_REQUEST_TOKEN, oAuthAccessRequest.getRequestToken());
        session.setAttribute(CoreSessionAttribute.OAUTH_REQUEST_SECRET, oAuthAccessRequest.getRequestSecret());
        return new ModelAndView("redirect:" + oAuthAccessRequest.getOAuthUrl());
    }

}