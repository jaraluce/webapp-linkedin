package me.relevante.web.controller;

/**
 * Created by daniel-ibanez on 17/07/16.
 */
public class LinkedinRoute {
    public static final String NETWORK = "/linkedin";
    public static final String LOGIN = NETWORK + "/login";
    public static final String LOGIN_CALLBACK = LOGIN + "/callback";
    public static final String API_GROUP = CoreRoute.API_PREFIX + NETWORK + "/groups/{groupId}";
    public static final String API_POST = CoreRoute.API_PREFIX + NETWORK + "/posts/{postId}";
    public static final String API_USER = CoreRoute.API_PREFIX + NETWORK + "/users/{userId}";
}
